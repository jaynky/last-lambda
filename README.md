August 2019 SPTA Updates

The focus of this past update was to make the site more responsive as well as filter out all unused classes in bootstrap.


# Cleaning Bootstrap

After manually copying and pasting the used bootstrap classes and finding it a tedious waste of time, I did some research and found purgeCSS. 
purgeCSS removes all unused CSS classes from a file. That is exactly what I wanted. 
Once you download it, the command is pretty simple:

`purgecss --css bootstrap.css --content index.html --out mincss`

where mincss is the name of the folder where the scrubbed CSS is placed.


>note: this requires you to download the bootstrap library and place it in your website's home directory [or copy paste the code here](https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css).

Read more about purgeCSS [on the official website](https://www.purgecss.com/).

# Responsive 1080p Magic

Yes that is a Bruno Mars reference. Making the site more responsive proved to be a little more challenging than I thought, but with a few adjustments and modifications everything seems to be working.

## DPR (Device Pixel Ratio)

Previously, I thought all that mattered in responsiveness is width. Turns out there is another dimension to consider, the pixel ratio. A device with too high a pixel ration will fail to display the adjusted media query rules for that screen size. To solve this issue I added the 
`and (-webkit-min-device-pixel-ratio: 1.0)`
to the @media query. 
In addition to adjusting the DPR, I added 
`<meta  name="viewport"  content="width=device-width, initial-scale=1.0, maximum-scale=2">`
to the `<head>` of the page.

## Refactoring CSS
Though not pertaining to making the site responsive, I changed the selectors for the top links from being classes to being based on the parents class and element.
`div.links  >  a`
Thus I was able to completely remove the toplink class from the HTML altogether. 


## New Rules

This is all the new CSS added

`/*Aug 2019 code*/

/* responsive magic */

@media  screen  and (max-width: 899px) and (-webkit-min-device-pixel-ratio: 1.0) {
	p {
	font-size: 1.5rem;
	}
	.main-div {
	padding: 0.2%;
	}
	.toplink {
	font-size: 2rem;
	}
}

/* cleaner replacement for the toplink class*/

div.links  >  a {
	background-color: #ac885e;
	color: #FFFFFF;
	padding: 10px  15px;
	text-align: center;
	text-decoration: none
	text-decoration-color: currentcolor;
	font-size: 16px;
	margin-left: 2%;
}

div.links  >  a:link {
	text-decoration: none;
}

div.links  >  a:hover {
	color: black;
}

.main-div {
	padding: 1%;
}`


